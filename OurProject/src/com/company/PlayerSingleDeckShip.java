package com.company;
import java.util.Scanner;

public class PlayerSingleDeckShip {
    byte NUMBER = 7;
    PlayerField playerField;
    public PlayerSingleDeckShip(PlayerField playerField){
        this.playerField = playerField;
    }

    public void addOnField(PlayerField playF, int FIELD_SIZE) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите координаты однопалубного корабля (всего: 4 корабля): ");
        String strX = sc.next();
        String strY = sc.next();
        if ((strX.equals("1") || strX.equals("2") || strX.equals("3") || (strX.equals("4") || strX.equals("5")
                || strX.equals("6")) || (strX.equals("7") || strX.equals("8") || strX.equals("9")) || strX.equals("0")) &&
                (((strY.equals("1") || strY.equals("2") || strY.equals("3") || (strY.equals("4") || strY.equals("5")
                        || strY.equals("6")) || (strY.equals("7") || strY.equals("8") || strY.equals("9") || strY.equals("0")))))) {
            int x = Integer.parseInt(strX);
            int y = Integer.parseInt(strY);
            if (playF.playerField[x][y] == 0) {
                playF.playerField[x][y] = 1;
                if (x - 1 > -1 && y - 1 > -1) {
                    playF.playerField[x - 1][y - 1] = NUMBER;
                }
                if (x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE) {
                    playF.playerField[x + 1][y + 1] = NUMBER;
                }
                if (x - 1 > -1 && y + 1 < FIELD_SIZE) {
                    playF.playerField[x - 1][y + 1] = NUMBER;
                }
                if (x + 1 < FIELD_SIZE && y - 1 > -1) {
                    playF.playerField[x + 1][y - 1] = NUMBER;
                }
                if (x - 1 > -1) {
                    playF.playerField[x - 1][y] = NUMBER;
                }
                if (x + 1 < FIELD_SIZE) {
                    playF.playerField[x + 1][y] = NUMBER;
                }
                if (y - 1 > -1) {
                    playF.playerField[x][y - 1] = NUMBER;
                }
                if (y + 1 < FIELD_SIZE) {
                    playF.playerField[x][y + 1] = NUMBER;
                }
            } else {
                System.out.println("Корабли нельзя ставить рядом");
                addOnField(playF, FIELD_SIZE);
            }
        } else {
            System.out.println("Вы ввели не правильную координату");
            addOnField(playF, FIELD_SIZE);
        }
    }
}
