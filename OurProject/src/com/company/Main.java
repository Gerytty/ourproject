package com.company;

public class Main {

    public static void main(String[] args) {

        PlayerField pf = new PlayerField();
        PlayerFieldShoot playerFieldShoot = new PlayerFieldShoot();
        playerFieldShoot.output();
        pf.addShips();

        ComputerFieldSingleton cf = new ComputerFieldSingleton();
        cf.getComputerField().addShips();

        PlayerShoot ps = new PlayerShoot(cf.getComputerField());

        ComputerShoot cs = new ComputerShoot(pf);


        while ((ps.count > 0) & (cs.count > 0)) {

//            if(ps.shoot() == false) {
//                cs.shoot();
//            }
//
//            if (cs.shoot() == false){
//                ps.shoot();
//            }
            ps.shoot();
            cs.shoot();
        }

    }
}
