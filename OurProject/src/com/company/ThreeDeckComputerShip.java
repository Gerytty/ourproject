package com.company;

import java.util.Random;

public class ThreeDeckComputerShip {
    ComputerField field;
    int NUMBER = 7;
    int COUNT_OF_DECKS = 3;
    public ThreeDeckComputerShip(ComputerField field) {
        this.field = field;
    }

    public void addOnField(ComputerField m, int FIELD_SIZE){
        Random random = new Random();
        int x = random.nextInt(FIELD_SIZE);
        if(x == FIELD_SIZE){
            x = x - 1;
        }
        int y = random.nextInt(FIELD_SIZE);
        if(y == FIELD_SIZE){
            y = y - 1;
        }
        int direction = random.nextInt(4);
        if(x - 1 > 0 && x - 2 > 0 && m.computerField[x][y] == 0 && m.computerField[x - 1][y] == 0 && direction == 0 && m.computerField[x - 2][y] == 0){
            m.computerField[x][y] = COUNT_OF_DECKS;
            m.computerField[x - 1][y] = COUNT_OF_DECKS;
            m.computerField[x - 2][y] = COUNT_OF_DECKS;
            if(y - 1 > -1){
                m.computerField[x][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE){
                m.computerField[x + 1][y] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(y + 1 < FIELD_SIZE){
                m.computerField[x][y + 1] = NUMBER;
            }
            if(x - 1 > -1 && y - 1 > -1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
            if(x - 2 > -1 && y - 1 > -1){
                m.computerField[x - 2][y - 1] = NUMBER;
            }
            if(x - 2 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 2][y + 1] = NUMBER;
            }
            if(x - 3 > -1 && y - 1 > -1){
                m.computerField[x - 3][y - 1] = NUMBER;
            }
            if(x - 3 > -1){
                m.computerField[x - 3][y] = NUMBER;
            }
            if(x - 3 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 3][y + 1] = NUMBER;
            }
        }


        else if(x + 1 < FIELD_SIZE && x + 2 < FIELD_SIZE && m.computerField[x][y] == 0 && m.computerField[x + 1][y] == 0 && m.computerField[x + 2][y] == 0 && direction == 1 ){
            m.computerField[x][y] = COUNT_OF_DECKS;
            m.computerField[x + 1][y] = COUNT_OF_DECKS;
            m.computerField[x + 2][y] = COUNT_OF_DECKS;
            if(x - 1 > -1 && y - 1 > - 1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1){
                m.computerField[x - 1][y] = NUMBER;
            }
            if(x - 1 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
            if(y - 1 > -1){
                m.computerField[x][y - 1] = NUMBER;
            }
            if(y + 1 < FIELD_SIZE){
                m.computerField[x][y + 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(x + 2 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 2][y - 1] = NUMBER;
            }
            if(x + 2 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 2][y + 1] = NUMBER;
            }
            if(x + 3 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 3][y - 1] = NUMBER;
            }
            if(x + 3 < FIELD_SIZE){
                m.computerField[x + 3][y] = NUMBER;
            }
            if(x + 3 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 3][y + 1] = NUMBER;
            }
        }
        else if(y + 1 < FIELD_SIZE && y + 2 < FIELD_SIZE && m.computerField[x][y] == 0 && m.computerField[x][y + 1] == 0  && m.computerField[x][y + 2] == 0&& direction == 2){
            m.computerField[x][y] = COUNT_OF_DECKS;
            m.computerField[x][y + 1] = COUNT_OF_DECKS;
            m.computerField[x][y + 2] = COUNT_OF_DECKS;
            if(x - 1 > -1 && y - 1 > -1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1){
                m.computerField[x - 1][y] = NUMBER;
            }
            if(x - 1 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
            if(x - 1 > -1 && y + 2 < FIELD_SIZE){
                m.computerField[x - 1][y + 2] = NUMBER;
            }
            if(y - 1 > -1){
                m.computerField[x][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE){
                m.computerField[x + 1][y] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 2 < FIELD_SIZE){
                m.computerField[x + 1][y + 2] = NUMBER;
            }
            if(x - 1 > -1 && y + 3 < FIELD_SIZE){
                m.computerField[x - 1][y + 3] = NUMBER;
            }
            if(y + 3 < FIELD_SIZE){
                m.computerField[x][y + 3] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 3 < FIELD_SIZE){
                m.computerField[x + 1][y + 3] = NUMBER;
            }
        }
        else if(y - 1 > 0 && y - 2 > 0 && m.computerField[x][y] == 0 && m.computerField[x][y - 1] == 0 && m.computerField[x][y - 2] == 0 && direction == 3) {
            m.computerField[x][y] = COUNT_OF_DECKS;
            m.computerField[x][y - 1] = COUNT_OF_DECKS;
            m.computerField[x][y - 2] = COUNT_OF_DECKS;
            if(x - 1 > -1 && y - 2 > -1){
                m.computerField[x - 1][y - 2] = NUMBER;
            }
            if(x - 1 > -1 && y - 1 > -1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1){
                m.computerField[x - 1][y] = NUMBER;
            }
            if(y + 1 < FIELD_SIZE){
                m.computerField[x][y + 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 2 > -1){
                m.computerField[x + 1][y - 2] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE){
                m.computerField[x + 1][y] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(x - 1 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
            if(x - 1 > -1 && y - 3 > -1){
                m.computerField[x - 1][y - 3] = NUMBER;
            }
            if(y - 3 > -1){
                m.computerField[x][y - 3] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 3 > -1){
                m.computerField[x + 1][y - 3] = NUMBER;
            }
        }

        else {
            addOnField(m, FIELD_SIZE);
        }
    }
}
