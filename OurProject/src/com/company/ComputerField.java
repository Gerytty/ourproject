package com.company;

public class ComputerField{
    public static final int FIELD_SIZE = 10;
    public int[][] computerField = new int[FIELD_SIZE][FIELD_SIZE];
    public int[][] playerShoots = new int[FIELD_SIZE][FIELD_SIZE]; //создан массив чтобы выводить данные выстрелов игрока в отдельное поле

//    public int[][] getComputerField() {
//        return computerField;
//    }
//
//    public void setComputerField1(int[][] computerField) {
//        this.computerField1 = computerField;
//    }

    public void addShips() {

        FourDeckComputerShip fs = new FourDeckComputerShip(this);
        fs.addOnField(this, FIELD_SIZE);

        ThreeDeckComputerShip ts1 = new ThreeDeckComputerShip(this);
        ts1.addOnField(this, FIELD_SIZE);

        ThreeDeckComputerShip ts2 = new ThreeDeckComputerShip(this);
        ts2.addOnField(this, FIELD_SIZE);

        DoubleDeckComputerShip ds1 = new DoubleDeckComputerShip(this);
        ds1.addOnField(this, FIELD_SIZE);

        DoubleDeckComputerShip ds2 = new DoubleDeckComputerShip(this);
        ds2.addOnField(this, FIELD_SIZE);

        DoubleDeckComputerShip ds3 = new DoubleDeckComputerShip(this);
        ds3.addOnField(this, FIELD_SIZE);

        SingleDeckComputerShip cs1 = new SingleDeckComputerShip(this);
        cs1.addOnField(this);

        SingleDeckComputerShip cs2 = new SingleDeckComputerShip(this);
        cs2.addOnField(this);

        SingleDeckComputerShip cs3 = new SingleDeckComputerShip(this);
        cs3.addOnField(this);

        SingleDeckComputerShip cs4 = new SingleDeckComputerShip(this);
        cs4.addOnField(this);
    }

    public void clearField(){
        for (int i = 0; i < FIELD_SIZE; i++) {
            for (int j = 0; j < FIELD_SIZE; j++) {
                if(computerField[i][j] == 7){
                    computerField[i][j] = 0;
                }
            }
        }
    }

    public void output () {
        clearField();
        for (int i = 0; i < FIELD_SIZE; i++) {
            for (int j = 0; j < FIELD_SIZE; j++) {
                System.out.print(computerField[i][j] + " ");
            }
            System.out.println();
        }
    }
    // метод выводит данные о выстрелах игрока в отдельном поле
    public void output1 () {
        clearField();
        for (int i = 0; i < FIELD_SIZE; i++) {
            for (int j = 0; j < FIELD_SIZE; j++) {
                if (computerField[i][j] == 5) {
                    playerShoots[i][j] = 5;
                }

                if (computerField[i][j] == 9) {
                    playerShoots[i][j] = 9;
                }
            }
        }
        System.out.println("ПОЛЕ ВЫСТРЕЛОВ ИГРОКА:");
        for (int i = 0; i < FIELD_SIZE; i++) {
            for (int j = 0; j < FIELD_SIZE; j++) {
                System.out.print(playerShoots[i][j] + " ");
            }
            System.out.println();
        }
    }

}
