package com.company;

import java.util.Random;

public class DoubleDeckComputerShip {
    ComputerField field;
    int NUMBER = 7;
    public DoubleDeckComputerShip(ComputerField field) {
        this.field = field;
    }

    public void addOnField(ComputerField m, int FIELD_SIZE){
        Random random = new Random();
        int x = random.nextInt(FIELD_SIZE);
        if(x == FIELD_SIZE){
            x = x - 1;
        }
        int y = random.nextInt(FIELD_SIZE);
        if(y == FIELD_SIZE){
            y = y - 1;
        }
        int direction = random.nextInt(4);
        if(x - 1 > 0 && m.computerField[x][y] == 0 && m.computerField[x - 1][y] == 0 && direction == 0){
            m.computerField[x][y] = 2;
            m.computerField[x - 1][y] = 2;
            if(y - 1 > -1){
                m.computerField[x][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE){
                m.computerField[x + 1][y] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(y + 1 < FIELD_SIZE){
                m.computerField[x][y + 1] = NUMBER;
            }
            if(x - 1 > -1 && y - 1 > -1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
            if(x - 2 > -1 && y - 1 > -1){
                m.computerField[x - 2][y - 1] = NUMBER;
            }
            if(x - 2 > -1){
                m.computerField[x - 2][y] = NUMBER;
            }
            if(x - 2 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 2][y + 1] = NUMBER;
            }
        }
        else if(x + 1 != FIELD_SIZE && m.computerField[x][y] == 0 && m.computerField[x + 1][y] == 0 && direction == 1 ){
            m.computerField[x][y] = 2;
            m.computerField[x + 1][y] = 2;
            if(x - 1 > -1 && y - 1 > - 1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1){
                m.computerField[x - 1][y] = NUMBER;
            }
            if(x - 1 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
            if(y - 1 > -1){
                m.computerField[x][y - 1] = NUMBER;
            }
            if(y + 1 < FIELD_SIZE){
                m.computerField[x][y + 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(x + 2 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 2][y - 1] = NUMBER;
            }
            if(x + 2 < FIELD_SIZE){
                m.computerField[x + 2][y] = NUMBER;
            }
            if(x + 2 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 2][y + 1] = NUMBER;
            }
        }
        else if(y + 1 != FIELD_SIZE && m.computerField[x][y] == 0 && m.computerField[x][y + 1] == 0 && direction == 2){
            m.computerField[x][y] = 2;
            m.computerField[x][y + 1] = 2;
            if(x - 1 > -1 && y - 1 > -1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1){
                m.computerField[x - 1][y] = NUMBER;
            }
            if(x - 1 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
            if(x - 1 > -1 && y + 2 < FIELD_SIZE){
                m.computerField[x - 1][y + 2] = NUMBER;
            }
            if(y - 1 > -1){
                m.computerField[x][y - 1] = NUMBER;
            }
            if(y + 2 < FIELD_SIZE){
                m.computerField[x][y + 2] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE){
                m.computerField[x + 1][y] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 2 < FIELD_SIZE){
                m.computerField[x + 1][y + 2] = NUMBER;
            }
        }
        else if(y - 1 > 0 && m.computerField[x][y] == 0 && m.computerField[x][y - 1] == 0 && direction == 3) {
            m.computerField[x][y] = 2;
            m.computerField[x][y - 1] = 2;
            if(x - 1 > -1 && y - 2 > -1){
                m.computerField[x - 1][y - 2] = NUMBER;
            }
            if(x - 1 > -1 && y - 1 > -1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1){
                m.computerField[x - 1][y] = NUMBER;
            }
            if(y - 2 > -1){
                m.computerField[x][y - 2] = NUMBER;
            }
            if(y + 1 < FIELD_SIZE){
                m.computerField[x][y + 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 2 > -1){
                m.computerField[x + 1][y - 2] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE){
                m.computerField[x + 1][y] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(x - 1 > -1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
        }
        else {
            addOnField(m, FIELD_SIZE);
        }
    }
}
