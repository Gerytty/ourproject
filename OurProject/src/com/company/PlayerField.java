package com.company;

public class PlayerField extends ComputerField{
    int FIELD_SIZE = 10;
    int[][] playerField = new int[FIELD_SIZE][FIELD_SIZE];
    public void addShips(){

        PlayerFourDeckShip pfd = new PlayerFourDeckShip(this);
        pfd.addOnField(this, FIELD_SIZE);
        output();

        PlayerThreeDeckShip ptd1 = new PlayerThreeDeckShip(this);
        ptd1.addOnField(this, FIELD_SIZE);
        output();

        PlayerThreeDeckShip ptd2 = new PlayerThreeDeckShip(this);
        ptd2.addOnField(this, FIELD_SIZE);
        output();

        PlayerDoubleDeckShip pdd1 = new PlayerDoubleDeckShip();
        pdd1.addOnField(this, FIELD_SIZE);
        output();

        PlayerDoubleDeckShip pdd2 = new PlayerDoubleDeckShip();
        pdd2.addOnField(this, FIELD_SIZE);
        output();

        PlayerDoubleDeckShip pdd3 = new PlayerDoubleDeckShip();
        pdd3.addOnField(this, FIELD_SIZE);
        output();

        PlayerSingleDeckShip psd1 = new PlayerSingleDeckShip(this);
        psd1.addOnField(this, FIELD_SIZE);
        output();

        PlayerSingleDeckShip psd2 = new PlayerSingleDeckShip(this);
        psd2.addOnField(this, FIELD_SIZE);
        output();

        PlayerSingleDeckShip psd3 = new PlayerSingleDeckShip(this);
        psd3.addOnField(this, FIELD_SIZE);
        output();

        PlayerSingleDeckShip psd4 = new PlayerSingleDeckShip(this);
        psd4.addOnField(this, FIELD_SIZE);
        clearField();
        output();
        System.out.println();
    }

    public void clearField(){
        for (int i = 0; i < FIELD_SIZE; i++) {
            for (int j = 0; j < FIELD_SIZE; j++) {
                if(playerField[i][j] == 7){
                    playerField[i][j] = 0;
                }
            }
        }
    }

    public void output () {
        System.out.println("ПОЛЕ КОРАБЛЕЙ ИГРОКА");
        for (int i = 0; i < FIELD_SIZE; i++) {
            for (int j = 0; j < FIELD_SIZE; j++) {
                System.out.print(playerField[i][j] + " ");
            }
            System.out.println();
        }
    }
}
