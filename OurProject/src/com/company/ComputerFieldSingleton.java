package com.company;

public class ComputerFieldSingleton{
    private static ComputerField computerFieldd = null;
    public static ComputerField getComputerField(){
        if(computerFieldd == null){
            computerFieldd = new ComputerField();
        }
        return computerFieldd;
    }
}
