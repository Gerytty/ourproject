package com.company;
import java.util.Scanner;

public class PlayerShoot implements InterfaceShoot {
    ComputerField field;
    int FIELD_SIZE = 10;
    boolean a = true;
    Scanner sc = new Scanner(System.in);
    int count = 10;

    public boolean shoot() {
        boolean a = true;
        while (a == true) {
            System.out.println("Введите координаты для стрельбы");
            int x1 = sc.nextInt();
            int y1 = sc.nextInt();
            field.output1();

            System.out.println("(ОТМЕНА выстрела - 2, подвердить - 1)");
            int d = sc.nextInt();

            if (d == 1) {
                int x = x1;
                int y = y1;

                if (field.computerField[x][y] == 9) {
                    System.out.println("Игрок: Мимо");
                    a = false;
                }

                if (field.computerField[x][y] == 5) {
                    System.out.println("Игрок: Мимо");
                    a = false;
                }

                if (field.computerField[x][y] == 1) {
                    System.out.println("Игрок: Убил однопалубный корабль");
                    field.computerField[x][y] = 5;
                    count--;
                }

                if (field.computerField[x][y] == 2) {
                    field.computerField[x][y] = 5;
                    if ((y - 1 >= 0 && field.computerField[x][y - 1] == 5) || (y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5) || (x - 1 >= 0 && field.computerField[x - 1][y] == 5) || (x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5)) {
                        System.out.println("Игрок: Убил двухпалубный корабль");
                        count--;
                    } else {
                        System.out.println("Игрок: Ранил двухпалубный корабль");
                    }
                }

                if (field.computerField[x][y] == 3) {
                    field.computerField[x][y] = 5;
                    if (((y - 1 >= 0 && field.computerField[x][y - 1] == 5) && (y - 2 >= 0 && field.computerField[x][y - 2] == 5)) || ((x - 1 >= 0 && field.computerField[x - 1][y] == 5) && (x - 2 >= 0 && field.computerField[x - 2][y] == 5)) ||
                            ((y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5) && (y - 1 >= 0 && field.computerField[x][y - 1] == 5)) || ((x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5) && (x - 1 >= 0 && field.computerField[x - 1][y] == 5))) {
                        System.out.println("Игрок: Убил трехпалубный корабль");
                        count--;

                    } else {
                        System.out.println("Игрок: Ранил трехпалубный корабль");
                    }
                }

                if (field.computerField[x][y] == 4) {
                    field.computerField[x][y] = 5;
                    if (((y - 1 >= 0 && field.computerField[x][y - 1] == 5) && (y - 2 >= 0 && field.computerField[x][y - 2] == 5) && (y - 3 >= 0 && field.computerField[x][y - 3] == 5)) ||
                            ((y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5) && (y - 1 >= 0 && field.computerField[x][y - 1] == 5) && (y - 2 >= 0 && field.computerField[x][y - 2] == 5)) ||
                            ((y + 2 < FIELD_SIZE && field.computerField[x][y + 2] == 5) && (y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5) && (y - 1 >= 0 && field.computerField[x][y - 1] == 5)) ||
                            ((y + 3 < FIELD_SIZE && field.computerField[x][y + 3] == 5) && (y + 2 < FIELD_SIZE && field.computerField[x][y + 2] == 5) && (y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5)) ||
                            ((x - 1 >= 0 && field.computerField[x - 1][y] == 5) && (x - 2 >= 0 && field.computerField[x - 2][y] == 5) && (x - 3 >= 0 && field.computerField[x - 3][y] == 5)) ||
                            ((x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5) && (x - 1 >= 0 && field.computerField[x - 1][y] == 5) && (x - 2 >= 0 && field.computerField[x - 2][y] == 5)) ||
                            ((x + 2 < FIELD_SIZE && field.computerField[x + 2][y] == 5) && (x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5) && (x - 1 >= 0 && field.computerField[x - 1][y] == 5)) ||
                            ((x + 3 < FIELD_SIZE && field.computerField[x + 3][y] == 5) && (x + 2 < FIELD_SIZE && field.computerField[x + 2][y] == 5) && (x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5))) {
                        System.out.println("Игрок: Убил четырехпалубный корабль");
                        count--;
                    } else {
                        System.out.println("Игрок: Ранил четырехпалубный корабль");
                    }

                }

                if (field.computerField[x][y] == 0) {
                    field.computerField[x][y] = 9;
                    System.out.println("Игрок: Мимо");
                    a = false;
                }


                if (field.computerField[x][y] == 7) {
                    System.out.println("Игрок: Мимо");
                    a = false;
                }


                field.output1();

            } //конец цикла d == 1

            if (d == 2) {
                System.out.println("Введите НОВЫЕ координаты для стрельбы");
                int x = sc.nextInt();
                int y = sc.nextInt();

                if (field.computerField[x][y] == 9) {
                    System.out.println("Игрок: Мимо");
                    a = false;
                }

                if (field.computerField[x][y] == 5) {
                    System.out.println("Игрок: Мимо");
                    a = false;
                }

                if (field.computerField[x][y] == 1) {
                    System.out.println("Игрок: Убил однопалубный корабль");
                    field.computerField[x][y] = 5;
                    count--;
                }

                if (field.computerField[x][y] == 2) {
                    field.computerField[x][y] = 5;
                    if ((y - 1 >= 0 && field.computerField[x][y - 1] == 5) || (y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5) || (x - 1 >= 0 && field.computerField[x - 1][y] == 5) || (x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5)) {
                        System.out.println("Игрок: Убил двухпалубный корабль");
                        count--;
                    } else {
                        System.out.println("Игрок: Ранил двухпалубный корабль");
                    }
                }

                if (field.computerField[x][y] == 3) {
                    field.computerField[x][y] = 5;
                    if (((y - 1 >= 0 && field.computerField[x][y - 1] == 5) && (y - 2 >= 0 && field.computerField[x][y - 2] == 5)) || ((x - 1 >= 0 && field.computerField[x - 1][y] == 5) && (x - 2 >= 0 && field.computerField[x - 2][y] == 5)) ||
                            ((y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5) && (y - 1 >= 0 && field.computerField[x][y - 1] == 5)) || ((x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5) && (x - 1 >= 0 && field.computerField[x - 1][y] == 5))) {
                        System.out.println("Игрок: Убил трехпалубный корабль");
                        count--;

                    } else {
                        System.out.println("Игрок: Ранил трехпалубный корабль");
                    }
                }

                if (field.computerField[x][y] == 4) {
                    field.computerField[x][y] = 5;
                    if (((y - 1 >= 0 && field.computerField[x][y - 1] == 5) && (y - 2 >= 0 && field.computerField[x][y - 2] == 5) && (y - 3 >= 0 && field.computerField[x][y - 3] == 5)) ||
                            ((y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5) && (y - 1 >= 0 && field.computerField[x][y - 1] == 5) && (y - 2 >= 0 && field.computerField[x][y - 2] == 5)) ||
                            ((y + 2 < FIELD_SIZE && field.computerField[x][y + 2] == 5) && (y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5) && (y - 1 >= 0 && field.computerField[x][y - 1] == 5)) ||
                            ((y + 3 < FIELD_SIZE && field.computerField[x][y + 3] == 5) && (y + 2 < FIELD_SIZE && field.computerField[x][y + 2] == 5) && (y + 1 < FIELD_SIZE && field.computerField[x][y + 1] == 5)) ||
                            ((x - 1 >= 0 && field.computerField[x - 1][y] == 5) && (x - 2 >= 0 && field.computerField[x - 2][y] == 5) && (x - 3 >= 0 && field.computerField[x - 3][y] == 5)) ||
                            ((x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5) && (x - 1 >= 0 && field.computerField[x - 1][y] == 5) && (x - 2 >= 0 && field.computerField[x - 2][y] == 5)) ||
                            ((x + 2 < FIELD_SIZE && field.computerField[x + 2][y] == 5) && (x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5) && (x - 1 >= 0 && field.computerField[x - 1][y] == 5)) ||
                            ((x + 3 < FIELD_SIZE && field.computerField[x + 3][y] == 5) && (x + 2 < FIELD_SIZE && field.computerField[x + 2][y] == 5) && (x + 1 < FIELD_SIZE && field.computerField[x + 1][y] == 5))) {
                        System.out.println("Игрок: Убил четырехпалубный корабль");
                        count--;
                    } else {
                        System.out.println("Игрок: Ранил четырехпалубный корабль");
                    }
                }

                if (field.computerField[x][y] == 0) {
                    field.computerField[x][y] = 9;
                    System.out.println("Игрок: Мимо");
                    a = false;
                }

                if (field.computerField[x][y] == 7) {
                    System.out.println("Игрок: Мимо");
                    a = false;
                }


                field.output1();

            } //конец цикла d == 2
        }
        return a;
    }

    public PlayerShoot(ComputerField field) {
        this.field = field;
    }


}