package com.company;

import java.util.Random;

public class SingleDeckComputerShip {
    public static final int FIELD_SIZE = 10;
    int x;
    int y;
    int NUMBER = 7;
    Random random = new Random();
    ComputerField field;

    public SingleDeckComputerShip(ComputerField field){
        this.field = field;
    }

    public void addOnField(ComputerField m){
        x = random.nextInt(FIELD_SIZE);
        y = random.nextInt(FIELD_SIZE);
        if(x == FIELD_SIZE){
            x = x - 1;
        }
        if(y == FIELD_SIZE){
            y = y - 1;
        }
        if(m.computerField[x][y] == 0 ){
            m.computerField[x][y] = 1;
            if(x - 1 > -1 && y - 1 > -1){
                m.computerField[x - 1][y - 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE){
                m.computerField[x + 1][y + 1] = NUMBER;
            }
            if(x - 1 > - 1 && y + 1 < FIELD_SIZE){
                m.computerField[x - 1][y + 1] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE && y - 1 > -1){
                m.computerField[x + 1][y - 1] = NUMBER;
            }
            if(x - 1 > -1){
                m.computerField[x - 1][y] = NUMBER;
            }
            if(x + 1 < FIELD_SIZE){
                m.computerField[x + 1][y] = NUMBER;
            }
            if(y - 1 > -1){
                m.computerField[x][y - 1] = NUMBER;
            }
            if(y + 1 < FIELD_SIZE){
                m.computerField[x][y + 1] = NUMBER;
            }
        }
        else addOnField(m);
    }
}