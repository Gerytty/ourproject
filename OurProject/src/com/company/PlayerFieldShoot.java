package com.company;
import java.util.Scanner;

public class PlayerFieldShoot {
    public static final int FIELD_SIZE = 10;
    public int[][] playerFieldShoot = new int[FIELD_SIZE][FIELD_SIZE];
    Scanner sc = new Scanner(System.in);

    public void output () {
        System.out.println ("ДОБРО ПОЖАЛОВАТЬ В ЛУЧШУЮ ИГРУ В СВОЕМ РОДЕ - МОРСКОЙ БОЙ!");
        System.out.println ("Это будет Ваше поле для выстрелов (0 - не было выстрела, 9 - был выстрел)");
        for (int i = 0; i < FIELD_SIZE; i++) {
            for (int j = 0; j < FIELD_SIZE; j++) {
                System.out.print(playerFieldShoot[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println ("А теперь расставьте свои корабли!");
    }

    public void addPlayerShoot () {
        int x = sc.nextInt();
        int y = sc.nextInt();
        playerFieldShoot[x][y] = 9;
        for (int i = 0; i < FIELD_SIZE; i++) {
            for (int j = 0; j < FIELD_SIZE; j++) {
                System.out.println(playerFieldShoot[i][j] + " ");
            }
        }
    }


}
