package com.company;
import java.util.Scanner;

public class PlayerDoubleDeckShip {
    byte x;
    byte y;
    int NUMBER = 7;
    PlayerField playerField;

    public void PlayerDoubleDeckShip(PlayerField playerField){

        this.playerField = playerField;
    }

    public void addOnField(PlayerField playF, int FIELD_SIZE) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите координаты двухпалубного корабля(всего: 3 корабля): ");
        String strX = sc.next();
        String strY = sc.next();
        if ((strX.equals("1") || strX.equals("2") || strX.equals("3") || (strX.equals("4") || strX.equals("5")
                || strX.equals("6")) || (strX.equals("7") || strX.equals("8") || strX.equals("9")) || strX.equals("0")) &&
                (((strY.equals("1") || strY.equals("2") || strY.equals("3") || (strY.equals("4") || strY.equals("5")
                        || strY.equals("6")) || (strY.equals("7") || strY.equals("8") || strY.equals("9") || strY.equals("0")))))) {
            int x = Integer.parseInt(strX);
            int y = Integer.parseInt(strY);
            if (playF.playerField[x][y] == 0) {
                System.out.println("Выберете направление(Вправао - r, влево - l, вниз - d, вверх - u)");
                char direction = sc.next().charAt(0);
                if (direction != 'u' && direction != 'd' && direction != 'l' && direction != 'r') {
                    System.out.println("Некорректное направление");
                    addOnField(playF, FIELD_SIZE);
                }
                if (direction == 'u') {
                    if (x - 1 > -1 && playF.playerField[x][y] == 0 && playF.playerField[x - 1][y] == 0) {
                        playF.playerField[x][y] = 2;
                        playF.playerField[x - 1][y] = 2;
                        if (y - 1 > -1) {
                            playF.playerField[x][y - 1] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y - 1 > -1) {
                            playF.playerField[x + 1][y - 1] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE) {
                            playF.playerField[x + 1][y] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE) {
                            playF.playerField[x + 1][y + 1] = NUMBER;
                        }
                        if (y + 1 < FIELD_SIZE) {
                            playF.playerField[x][y + 1] = NUMBER;
                        }
                        if (x - 1 > -1 && y - 1 > -1) {
                            playF.playerField[x - 1][y - 1] = NUMBER;
                        }
                        if (x - 1 > -1 && y + 1 < FIELD_SIZE) {
                            playF.playerField[x - 1][y + 1] = NUMBER;
                        }
                        if (x - 2 > -1 && y - 1 > -1) {
                            playF.playerField[x - 2][y - 1] = NUMBER;
                        }
                        if (x - 2 > -1) {
                            playF.playerField[x - 2][y] = NUMBER;
                        }
                        if (x - 2 > -1 && y + 1 < FIELD_SIZE) {
                            playF.playerField[x - 2][y + 1] = NUMBER;
                        }
                    } else {
                        System.out.println("Корабли не должны накладываться друг на друга или выходить за переделы поля, попробуйте ещё раз");
                        addOnField(playF, FIELD_SIZE);
                    }

                } else if (direction == 'd') {
                    if (x + 1 < FIELD_SIZE && playF.playerField[x][y] == 0 && playF.playerField[x + 1][y] == 0) {
                        playF.playerField[x][y] = 2;
                        playF.playerField[x + 1][y] = 2;
                        if (x - 1 > -1 && y - 1 > -1) {
                            playF.playerField[x - 1][y - 1] = NUMBER;
                        }
                        if (x - 1 > -1) {
                            playF.playerField[x - 1][y] = NUMBER;
                        }
                        if (x - 1 > -1 && y + 1 < FIELD_SIZE) {
                            playF.playerField[x - 1][y + 1] = NUMBER;
                        }
                        if (y - 1 > -1) {
                            playF.playerField[x][y - 1] = NUMBER;
                        }
                        if (y + 1 < FIELD_SIZE) {
                            playF.playerField[x][y + 1] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y - 1 > -1) {
                            playF.playerField[x + 1][y - 1] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE) {
                            playF.playerField[x + 1][y + 1] = NUMBER;
                        }
                        if (x + 2 < FIELD_SIZE && y - 1 > -1) {
                            playF.playerField[x + 2][y - 1] = NUMBER;
                        }
                        if (x + 2 < FIELD_SIZE) {
                            playF.playerField[x + 2][y] = NUMBER;
                        }
                        if (x + 2 < FIELD_SIZE && y + 1 < FIELD_SIZE) {
                            playF.playerField[x + 2][y + 1] = NUMBER;
                        }
                    } else {
                        System.out.println("Корабли не должны накладываться друг на друга или выходить за переделы поля, попробуйте ещё раз");
                        addOnField(playF, FIELD_SIZE);
                    }

                } else if (direction == 'r') {
                    if (y + 1 < FIELD_SIZE && playF.playerField[x][y] == 0 && playF.playerField[x][y + 1] == 0) {
                        playF.playerField[x][y] = 2;
                        playF.playerField[x][y + 1] = 2;
                        if (x - 1 > -1 && y - 1 > -1) {
                            playF.playerField[x - 1][y - 1] = NUMBER;
                        }
                        if (x - 1 > -1) {
                            playF.playerField[x - 1][y] = NUMBER;
                        }
                        if (x - 1 > -1 && y + 1 < FIELD_SIZE) {
                            playF.playerField[x - 1][y + 1] = NUMBER;
                        }
                        if (x - 1 > -1 && y + 2 < FIELD_SIZE) {
                            playF.playerField[x - 1][y + 2] = NUMBER;
                        }
                        if (y - 1 > -1) {
                            playF.playerField[x][y - 1] = NUMBER;
                        }
                        if (y + 2 < FIELD_SIZE) {
                            playF.playerField[x][y + 2] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y - 1 > -1) {
                            playF.playerField[x + 1][y - 1] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE) {
                            playF.playerField[x + 1][y] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE) {
                            playF.playerField[x + 1][y + 1] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y + 2 < FIELD_SIZE) {
                            playF.playerField[x + 1][y + 2] = NUMBER;
                        }
                    } else {
                        System.out.println("Корабли не должны накладываться друг на друга или выходить за переделы поля, попробуйте ещё раз");
                        addOnField(playF, FIELD_SIZE);
                    }

                } else if (direction == 'l') {
                    if (y - 1 > 0 && playF.playerField[x][y] == 0 && playF.playerField[x][y - 1] == 0) {
                        playF.playerField[x][y] = 2;
                        playF.playerField[x][y - 1] = 2;
                        if (x - 1 > -1 && y - 2 > -1) {
                            playF.playerField[x - 1][y - 2] = NUMBER;
                        }
                        if (x - 1 > -1 && y - 1 > -1) {
                            playF.playerField[x - 1][y - 1] = NUMBER;
                        }
                        if (x - 1 > -1) {
                            playF.playerField[x - 1][y] = NUMBER;
                        }
                        if (y - 2 > -1) {
                            playF.playerField[x][y - 2] = NUMBER;
                        }
                        if (y + 1 < FIELD_SIZE) {
                            playF.playerField[x][y + 1] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y - 2 > -1) {
                            playF.playerField[x + 1][y - 2] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y - 1 > -1) {
                            playF.playerField[x + 1][y - 1] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE) {
                            playF.playerField[x + 1][y] = NUMBER;
                        }
                        if (x + 1 < FIELD_SIZE && y + 1 < FIELD_SIZE) {
                            playF.playerField[x + 1][y + 1] = NUMBER;
                        }
                        if (x - 1 > -1 && y + 1 < FIELD_SIZE) {
                            playF.playerField[x - 1][y + 1] = NUMBER;
                        }
                    } else {
                        System.out.println("Корабли не должны накладываться друг на друга или выходить за переделы поля, попробуйте ещё раз");
                        addOnField(playF, FIELD_SIZE);
                    }
                }
            } else {
                System.out.println("Корабли не должны накладываться друг на друга");
                addOnField(playF, FIELD_SIZE);
            }
        } else {
            System.out.println("Вы ввели не правильную координату");
            addOnField(playF, FIELD_SIZE);
        }
    }
}
